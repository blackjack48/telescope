﻿ThumbsFS = new CollectionFS('thumbs');

Meteor.methods({
  createThumbnail: function(id, url){
    //invokes the server-side thumbnail retrieval process
    Meteor.call('processThumbnail', id, url);
  }
});
