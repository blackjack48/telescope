﻿Meteor.methods({
  processThumbnail: function (id, url) {
    var request = Npm.require('request').defaults({ encoding: null });
    Future = Npm.require('fibers/future');
    var embedlyRespUrl = '';
    var fut = new Future();
    //send URL to embedly
    request.get('http://api.embed.ly/1/extract?key=5c304f87ea8d48a78667a0ba2c9a6885&url='+encodeURI(url)+'&format=json', {encoding: 'utf-8'}, Meteor.bindEnvironment(function (error, result, body) {
      if (!error && result.statusCode == 200) {
        console.log(body);
        var obj = JSON.parse(body);
        if (typeof obj.images[0].url !== 'undefined')
          fut.return(obj.images[0].url);
        else
          fut.return(false);
      }
    }, function () { console.log('Failed to bind environment'); }));

    embedlyRespUrl = fut.wait();

    if (embedlyRespUrl) {
      //download embedly image
      request.get(embedlyRespUrl, Meteor.bindEnvironment(function (error, result, body) {
        if (!error && result.statusCode == 200) {
          console.log(body);
          ThumbsFS.storeBuffer(id, body, {
            contentType: result.headers['content-type'],
            noProgress: true,
            encoding: 'binary'
          });
        }
      }, function () { console.log('Failed to bind environment (2)'); }));
    }
  }
});
